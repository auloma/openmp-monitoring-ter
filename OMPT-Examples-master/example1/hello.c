#include <omp.h>
#include <stdio.h>
#ifndef DYN_TOOL
#include "initialization.h"
#endif

int main() {
#pragma omp parallel num_threads(4)
  {
    printf("Hello from thread %i of %i!\n", omp_get_thread_num(),
           omp_get_num_threads());
  }
  return 0;
}
