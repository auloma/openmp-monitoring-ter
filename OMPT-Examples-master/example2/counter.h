#include <stdio.h>
#include <inttypes.h>
#include <omp.h>
#include <omp-tools.h>

// 240 should be enough for xeon-phi
#define MAX_THREADS 240

#define CACHE_LINE 128
#define KMP_PAD(type, sz)     (sizeof(type) + (sz - ((sizeof(type) - 1) % (sz)) - 1))


typedef struct {
    int thread_begin;				// (1)	thread_begin
    int thread_end; 				// (2)	thread_end
    int teams_begin;
    int teams_end;
    int parallel_begin;				// (3)	parallel_begin
    int parallel_end;				// (4)	parallel_end
    int task_create_initial;			// (5)	task_create: 	task_initial
    int task_create_implicit;
    int task_create_explicit;			//			task_explicit
    int task_create_target;			//			task_target
    int task_create_taskwait;
    int task_create_undeferred;			//			task_undeferred
    int task_create_untied;			//			task_untied
    int task_create_final;
    int task_create_mergeable;
    int task_create_merged;
    int task_schedule;				// (6)	task_schedule 
    int implicit_task_scope_begin;		// (7)	implicit task:	scope_begin
    int implicit_task_scope_end;		//			scope_end
    int initial_task_scope_begin;		
    int initial_task_scope_end;	
}callback_counter;

typedef union __attribute__((aligned(CACHE_LINE))) callback_counter_u {
  double           c_align;        /* use worst case alignment */
  char             c_pad[ KMP_PAD(callback_counter, CACHE_LINE) ];
  callback_counter cc;
} callback_counter_t;

void sum_up_callbacks(callback_counter_t *counter);
