#include "counter.h"

#define OUTPUT_IF_NOT_NULL(format, value)                                      \
  if (value)                                                                   \
  printf(format, value)

void sum_up_callbacks(callback_counter_t *counter) {
  for (int i = 1; i < MAX_THREADS; i++) {
    // if we have at least i threads, this thread should have a thread_begin
    // event:
    if (counter[i].cc.thread_begin == 0)
      break;
    counter[0].cc.thread_begin += counter[i].cc.thread_begin;
    counter[0].cc.thread_end += counter[i].cc.thread_end;
    counter[0].cc.teams_begin += counter[i].cc.teams_begin;
    counter[0].cc.teams_end += counter[i].cc.teams_end;
    counter[0].cc.parallel_begin += counter[i].cc.parallel_begin;
    counter[0].cc.parallel_end += counter[i].cc.parallel_end;
    counter[0].cc.task_create_initial += counter[i].cc.task_create_initial;
    counter[0].cc.task_create_implicit += counter[i].cc.task_create_implicit;
    counter[0].cc.task_create_explicit += counter[i].cc.task_create_explicit;
    counter[0].cc.task_create_target += counter[i].cc.task_create_target;
    counter[0].cc.task_create_taskwait += counter[i].cc.task_create_taskwait;
    counter[0].cc.task_create_undeferred +=
        counter[i].cc.task_create_undeferred;
    counter[0].cc.task_create_untied += counter[i].cc.task_create_untied;
    counter[0].cc.task_create_final += counter[i].cc.task_create_final;
    counter[0].cc.task_create_mergeable += counter[i].cc.task_create_mergeable;
    counter[0].cc.task_create_merged += counter[i].cc.task_create_merged;
    counter[0].cc.task_schedule += counter[i].cc.task_schedule;
    counter[0].cc.implicit_task_scope_begin +=
        counter[i].cc.implicit_task_scope_begin;
    counter[0].cc.implicit_task_scope_end +=
        counter[i].cc.implicit_task_scope_end;
    counter[0].cc.initial_task_scope_begin +=
        counter[i].cc.initial_task_scope_begin;
    counter[0].cc.initial_task_scope_end +=
        counter[i].cc.initial_task_scope_end;
  }

  int total_callbacks =
      counter[0].cc.thread_begin + counter[0].cc.thread_end +
      counter[0].cc.teams_begin + counter[0].cc.teams_end +
      counter[0].cc.parallel_begin + counter[0].cc.parallel_end +
      counter[0].cc.task_create_initial + counter[0].cc.task_create_implicit +
      counter[0].cc.task_create_explicit + counter[0].cc.task_create_target +
      counter[0].cc.task_create_taskwait +
      counter[0].cc.task_create_undeferred + counter[0].cc.task_create_untied +
      counter[0].cc.task_create_final + counter[0].cc.task_create_mergeable +
      counter[0].cc.task_create_merged + counter[0].cc.task_schedule +
      counter[0].cc.implicit_task_scope_begin +
      counter[0].cc.implicit_task_scope_end +
      counter[0].cc.initial_task_scope_begin +
      counter[0].cc.initial_task_scope_end;

  printf("Total callbacks: %d\n", total_callbacks);
  printf("--------------------------------------\n");
  OUTPUT_IF_NOT_NULL("%5d thread_begin\n", counter[0].cc.thread_begin);
  OUTPUT_IF_NOT_NULL("%5d thread_end\n", counter[0].cc.thread_end);
  OUTPUT_IF_NOT_NULL("%5d teams_begin\n", counter[0].cc.teams_begin);
  OUTPUT_IF_NOT_NULL("%5d teams_end\n", counter[0].cc.teams_end);
  OUTPUT_IF_NOT_NULL("%5d parallel_begin\n", counter[0].cc.parallel_begin);
  OUTPUT_IF_NOT_NULL("%5d parallel_end\n", counter[0].cc.parallel_end);
  OUTPUT_IF_NOT_NULL("%5d task_create : initial\n",
                     counter[0].cc.task_create_initial);
  OUTPUT_IF_NOT_NULL("%5d task_create : implicit\n",
                     counter[0].cc.task_create_implicit);
  OUTPUT_IF_NOT_NULL("%5d task_create : explicit\n",
                     counter[0].cc.task_create_explicit);
  OUTPUT_IF_NOT_NULL("%5d task_create : target\n",
                     counter[0].cc.task_create_target);
  OUTPUT_IF_NOT_NULL("%5d task_create : taskwait\n",
                     counter[0].cc.task_create_taskwait);
  OUTPUT_IF_NOT_NULL("%5d task_create : undeferred\n",
                     counter[0].cc.task_create_undeferred);
  OUTPUT_IF_NOT_NULL("%5d task_create : untied\n",
                     counter[0].cc.task_create_untied);
  OUTPUT_IF_NOT_NULL("%5d task_create : final\n",
                     counter[0].cc.task_create_final);
  OUTPUT_IF_NOT_NULL("%5d task_create : mergeable\n",
                     counter[0].cc.task_create_mergeable);
  OUTPUT_IF_NOT_NULL("%5d task_create : merged\n",
                     counter[0].cc.task_create_merged);
  OUTPUT_IF_NOT_NULL("%5d task_schedule\n", counter[0].cc.task_schedule);
  OUTPUT_IF_NOT_NULL("%5d implicit_task : scope_begin\n",
                     counter[0].cc.implicit_task_scope_begin);
  OUTPUT_IF_NOT_NULL("%5d implicit_task : scope_end\n",
                     counter[0].cc.implicit_task_scope_end);
  OUTPUT_IF_NOT_NULL("%5d initial_task : scope_begin\n",
                     counter[0].cc.initial_task_scope_begin);
  OUTPUT_IF_NOT_NULL("%5d initial_task : scope_end\n",
                     counter[0].cc.initial_task_scope_end);

  return;
}
