#include <omp.h>
#include <stdio.h>

int main() {
#pragma omp task
  {
    printf("task\n");
#pragma omp task
    { printf("nested task\n"); }
  }

#pragma omp parallel num_threads(2)
  {
#pragma omp single
#pragma omp task
    {
      printf("task in parallel\n");
#pragma omp task
      { printf("nested task in parallel\n"); }
    }
  }

  return 0;
}
