#include "counter.h"
#include <assert.h>
#include <execinfo.h>
#include <inttypes.h>
#include <omp.h>
#include <omp-tools.h>
#include <stdio.h>
#include <sys/resource.h>

static ompt_get_thread_data_t ompt_get_thread_data;
static ompt_get_unique_id_t ompt_get_unique_id;

callback_counter_t *counter;

static uint64_t my_next_id() {
  static uint64_t ID = 0;
  uint64_t ret = __sync_fetch_and_add(&ID, 1);
  assert(ret < MAX_THREADS &&
         "Maximum number of allowed threads is limited by MAX_THREADS");
  return ret;
}

static int max_tasks;
static int queued_tasks;

static void on_ompt_callback_implicit_task(ompt_scope_endpoint_t endpoint,
                                           ompt_data_t *parallel_data,
                                           ompt_data_t *task_data,
                                           unsigned int actual_parallelism,
                                           unsigned int index, int flags) {
  uint64_t tid = ompt_get_thread_data()->value;
  if (flags & ompt_task_implicit) {
    switch (endpoint) {
    case ompt_scope_begin:
      counter[tid].cc.implicit_task_scope_begin += 1;
      task_data->value = ompt_get_unique_id();
      break;
    case ompt_scope_end:
      counter[tid].cc.implicit_task_scope_end += 1;
      break;
    // case ompt_scope_beginend:
    //   assert(0 && "Should never happen in ompt_callback_implicit_task");
    //   break;
    }
  } else if (flags & ompt_task_initial) {
    switch (endpoint) {
    case ompt_scope_begin:
      counter[tid].cc.initial_task_scope_begin += 1;
      task_data->value = ompt_get_unique_id();
      break;
    case ompt_scope_end:
      counter[tid].cc.initial_task_scope_end += 1;
      break;
    // case ompt_scope_beginend:
    //   assert(0 && "Should never happen in ompt_callback_implicit_task");
    //   break;
    }
  }
}

static void on_ompt_callback_parallel_begin(
    ompt_data_t *encountering_task_data,
    const ompt_frame_t *encountering_task_frame, ompt_data_t *parallel_data,
    uint32_t requested_parallelism, int flags, const void *codeptr_ra) {
  uint64_t tid = ompt_get_thread_data()->value;
  if (flags & ompt_parallel_league) {
    counter[tid].cc.teams_begin += 1;
  } else if (flags & ompt_parallel_team) { // only 'else' sufficient
    counter[tid].cc.parallel_begin += 1;
  }
  parallel_data->value = ompt_get_unique_id();
}

static void on_ompt_callback_parallel_end(ompt_data_t *parallel_data,
                                          ompt_data_t *encountering_task_data,
                                          int flags, const void *codeptr_ra) {
  uint64_t tid = ompt_get_thread_data()->value;
  if (flags & ompt_parallel_league) {
    counter[tid].cc.teams_end += 1;
  } else if (flags & ompt_parallel_team) { // only 'else' sufficient
    counter[tid].cc.parallel_end += 1;
  }
}

static void on_ompt_callback_task_create(
    ompt_data_t *encountering_task_data, /* id of parent task            */
    const ompt_frame_t
        *encountering_task_frame, /* frame data for parent task   */
    ompt_data_t *new_task_data,   /* id of created task           */
    int flags, int has_dependences,
    const void *codeptr_ra) /* pointer to outlined function */
{
  uint64_t tid = ompt_get_thread_data()->value;
  if (flags & ompt_task_explicit)
    counter[tid].cc.task_create_explicit += 1;
  if (flags & ompt_task_target)
    counter[tid].cc.task_create_target += 1;
  if (flags & ompt_task_taskwait)
    counter[tid].cc.task_create_taskwait += 1;
  if (flags & ompt_task_undeferred)
    counter[tid].cc.task_create_undeferred += 1;
  if (flags & ompt_task_untied)
    counter[tid].cc.task_create_untied += 1;
  if (flags & ompt_task_final)
    counter[tid].cc.task_create_final += 1;
  if (flags & ompt_task_mergeable)
    counter[tid].cc.task_create_mergeable += 1;
  if (flags & ompt_task_merged)
    counter[tid].cc.task_create_merged += 1;
  // this creates a statistic for maximal concurrent tasks in the whole
  // application:
  int taskqueue = __sync_add_and_fetch(&queued_tasks, 1);
  // max_tasks = max(max_tasks, taskqueue)
  int maxqueue = __sync_fetch_and_add(&max_tasks, 0);
  while (taskqueue > maxqueue)
    maxqueue = __sync_val_compare_and_swap(&max_tasks, maxqueue, taskqueue);

  new_task_data->value = ompt_get_unique_id();
}

static void on_ompt_callback_task_schedule(ompt_data_t *prior_task_data,
                                           ompt_task_status_t prior_task_status,
                                           ompt_data_t *next_task_data) {
  uint64_t tid = ompt_get_thread_data()->value;
  counter[tid].cc.task_schedule += 1;
  if (prior_task_status == ompt_task_complete ||
      prior_task_status == ompt_task_cancel ||
      prior_task_status == ompt_task_late_fulfill) {
    __sync_fetch_and_sub(&queued_tasks, 1);
  }
}

static void on_ompt_callback_thread_begin(ompt_thread_t thread_type,
                                          ompt_data_t *thread_data) {
  uint64_t tid = thread_data->value = my_next_id();
  counter[tid].cc.thread_begin += 1;
}

static void on_ompt_callback_thread_end(ompt_data_t *thread_data) {
  uint64_t tid = thread_data->value;
  counter[tid].cc.thread_end += 1;
}

#define register_callback_t(name, type)                                        \
  do {                                                                         \
    type f_##name = &on_##name;                                                \
    if (ompt_set_callback(name, (ompt_callback_t)f_##name) == ompt_set_never)  \
      printf("0: Could not register callback '" #name "'\n");                  \
  } while (0)

#define register_callback(name) register_callback_t(name, name##_t)

int ompt_initialize(ompt_function_lookup_t lookup, int initial_device_num,
                    ompt_data_t *tool_data) {
  ompt_set_callback_t ompt_set_callback =
      (ompt_set_callback_t)lookup("ompt_set_callback");
  ompt_get_thread_data = (ompt_get_thread_data_t)lookup("ompt_get_thread_data");
  ompt_get_unique_id = (ompt_get_unique_id_t)lookup("ompt_get_unique_id");

  register_callback(ompt_callback_implicit_task);
  register_callback(ompt_callback_parallel_begin);
  register_callback(ompt_callback_parallel_end);
  register_callback(ompt_callback_task_create);
  register_callback(ompt_callback_task_schedule);
  register_callback(ompt_callback_thread_begin);
  register_callback(ompt_callback_thread_end);

  counter = calloc(MAX_THREADS, sizeof(callback_counter_t));
  tool_data->ptr = counter;
  return 1; // success
}

void ompt_finalize(ompt_data_t *tool_data) {
  struct rusage end;
  getrusage(RUSAGE_SELF, &end);
  printf("MAX RSS[KBytes] during execution: %ld\n", end.ru_maxrss);
  // runtime shutdown
  sum_up_callbacks(tool_data->ptr);
  printf("Max Tasks in queue: %i\n", max_tasks);

  free(tool_data->ptr);
}

ompt_start_tool_result_t *ompt_start_tool(unsigned int omp_version,
                                          const char *runtime_version) {
  static ompt_start_tool_result_t ompt_start_tool_result = {
      &ompt_initialize, &ompt_finalize, {.value = 0}};
  return &ompt_start_tool_result;
}
