#ifdef USE_OMPDT
#define CLIENT_TOOL_LIBRARIES_VAR "SAMPLE_TOOL_LIBRARIES"
#include "ompt_multiplex.h"
#include "ompdt.h"
#endif

#include <omp.h>
#include <omp-tools.h>

#include <map>
#include <cstring>
#include <string>

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <inttypes.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/syscall.h>


static ompt_enumerate_states_t ompt_enumerate_states;
static ompt_get_thread_data_t ompt_get_thread_data;
static ompt_get_state_t ompt_get_state;
static ompt_get_task_info_t ompt_get_task_info;
static ompt_get_unique_id_t ompt_get_unique_id;

typedef std::map<int, std::pair<std::string, int>> stateMap;
stateMap ompt_state_map;

struct threadData{
  uint64_t tid;
  stateMap ompt_thread_state_map;
  
  threadData(uint64_t Tid, stateMap& map):tid(Tid), ompt_thread_state_map(map){}
};

static uint64_t my_next_id()
{
  static uint64_t ID=0;
  uint64_t ret = __sync_fetch_and_add(&ID,1);
  return ret;
}


#ifdef SYS_gettid
pid_t getTid(){
  return syscall(SYS_gettid);
}
#else
#error "SYS_gettid unavailable on this system"
#endif

static void
on_ompt_callback_implicit_task(
    ompt_scope_endpoint_t endpoint,
    ompt_data_t *parallel_data,
    ompt_data_t *task_data,
    unsigned int team_size,
    unsigned int thread_num)
{
  if (endpoint == ompt_scope_begin)
      task_data->value = ompt_get_unique_id();
}

static void
on_ompt_callback_parallel_begin(
  ompt_data_t *parent_task_data,
  const ompt_frame_t *parent_task_frame,
  ompt_data_t* parallel_data,
  uint32_t requested_team_size,
  ompt_invoker_t invoker,
  const void *codeptr_ra)
{
  parallel_data->value = ompt_get_unique_id();
}

static void
on_ompt_callback_task_create(
    ompt_data_t *parent_task_data,    /* id of parent task            */
    const ompt_frame_t *parent_frame,  /* frame data for parent task   */
    ompt_data_t* new_task_data,      /* id of created task           */
    int type,
    int has_dependences,
    const void *codeptr_ra)               /* pointer to outlined function */
{
  new_task_data->value = ompt_get_unique_id();
}

#ifdef USE_OMPDT
void taskTrace(){
    if (!ompt_multiplex_client_callbacks.ompt_callback_control_tool) return;
    char outputBuffer[2048];
    outputBuffer[0] = '\0';
    ompdtBuffer buffer = {.buffer=(void*)outputBuffer, .size = 2048};
    ompt_multiplex_client_callbacks.ompt_callback_control_tool(ompdt_command_get, ompdt_modifier_get_task_histroy_trace, &buffer, NULL);
    if (outputBuffer[0] != '\0') 
      printf("Task history trace:\n%s\n\n", outputBuffer);
}

void fullTrace(){
    if (!ompt_multiplex_client_callbacks.ompt_callback_control_tool) return;
    char outputBuffer[2048];
    outputBuffer[0] = '\0';
    ompdtBuffer buffer = {.buffer=(void*)outputBuffer, .size = 2048};
    ompt_multiplex_client_callbacks.ompt_callback_control_tool(ompdt_command_get, ompdt_modifier_get_full_stack_trace, &buffer, NULL);
    if (outputBuffer[0] != '\0') 
      printf("Full Stacktrace:\n%s\n\n", outputBuffer);
}

void strippedTrace(){
    if (!ompt_multiplex_client_callbacks.ompt_callback_control_tool) return;
    char outputBuffer[2048];
    outputBuffer[0] = '\0';
    ompdtBuffer buffer = {.buffer=(void*)outputBuffer, .size = 2048};
    ompt_multiplex_client_callbacks.ompt_callback_control_tool(ompdt_command_get, ompdt_modifier_get_stripped_stack_trace, &buffer, NULL);
    if (outputBuffer[0] != '\0') 
      printf("Stripped Stacktrace:\n%s\n\n", outputBuffer);
}
#endif

static void
handler(int sig, siginfo_t *si, void *uc)
{
//  printf("Caught signal %d\n", sig);
  if (!ompt_get_thread_data || !ompt_get_state) return;
  ompt_data_t *data = ompt_get_thread_data();
  if (!data) return;
  threadData *thread_data = (threadData*)(data->ptr);
  ompt_wait_id_t waitId;
  omp_state_t state = ompt_get_state(&waitId);
  thread_data->ompt_thread_state_map[state].second++;
  #ifdef USE_OMPDT
  if (thread_data->ompt_thread_state_map[state].second % 100 == 1)
    taskTrace();
  #endif
}

#define errExit(msg)    do { perror(msg); exit(EXIT_FAILURE);} while (0)
#define SIG SIGALRM
static void
on_ompt_callback_thread_begin(
  ompt_thread_type_t thread_type,
  ompt_data_t *thread_data)
{
  thread_data->ptr = new threadData(my_next_id(), ompt_state_map);
  timer_t timerid;
  struct sigevent sev;
  struct itimerspec its;
  long long freq_nanosecs;
  sigset_t mask;
  struct sigaction sa;
  
  /* Establish handler for timer signal */

  printf("Establishing handler for signal %d on thread %d\n", SIG, getTid());
  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = handler;
  sigemptyset(&sa.sa_mask);
  if (sigaction(SIG, &sa, NULL) == -1)
    errExit("sigaction");

  printf("Blocking signal %d\n", SIG);
  sigemptyset(&mask);
  sigaddset(&mask, SIG);
  if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
    errExit("sigprocmask");

  /* Create the timer */

  sev.sigev_notify = SIGEV_THREAD_ID;
  sev.sigev_signo = SIG;
  sev.sigev_value.sival_ptr = &timerid;
  sev._sigev_un._tid=getTid();
  if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1)
    errExit("timer_create");

  printf("timer ID is 0x%lx\n", (long) timerid);

  /* Start the timer */

  its.it_interval.tv_sec = its.it_value.tv_sec = 0;
  its.it_interval.tv_nsec = its.it_value.tv_nsec = 100000;

  if (timer_settime(timerid, 0, &its, NULL) == -1)
    errExit("timer_settime");

  /* Unlock the timer signal, so that timer notification
     can be delivered */

  printf("Unblocking signal %d\n", SIG);
  if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1)
    errExit("sigprocmask");

}

static void
on_ompt_callback_thread_end(
  ompt_data_t *thread_data)
{
  if (!thread_data) return;
  threadData *data = (threadData *)(thread_data->ptr);
/*  if(data->ompt_thread_state_map.empty())
    printf("Thread %lu: no sample taken\n", data->tid);*/
  for(auto i: data->ompt_thread_state_map)
    if (i.second.second)
      printf("Thread %lu: %i \"%s\"(%i)\n", data->tid, i.second.second, i.second.first.c_str(), i.first);
  free(thread_data->ptr);
//  delete_data(thread_data);
}

#define register_callback_t(name, type)                       \
do{                                                           \
  type f_##name = &on_##name;                                 \
  if (ompt_set_callback(name, (ompt_callback_t)f_##name) ==   \
      ompt_set_never)                                         \
    printf("0: Could not register callback '" #name "'\n");   \
}while(0)

#define register_callback(name) register_callback_t(name, name##_t)

int ompt_initialize(
  ompt_function_lookup_t lookup,
  ompt_data_t* data)
{
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_get_thread_data = (ompt_get_thread_data_t) lookup("ompt_get_thread_data");
  ompt_get_state = (ompt_get_state_t) lookup("ompt_get_state");
  ompt_enumerate_states = (ompt_enumerate_states_t) lookup("ompt_enumerate_states");
  ompt_get_task_info = (ompt_get_task_info_t) lookup("ompt_get_task_info");
  ompt_get_unique_id = (ompt_get_unique_id_t) lookup("ompt_get_unique_id");

  register_callback(ompt_callback_implicit_task);
  register_callback(ompt_callback_parallel_begin);
  register_callback(ompt_callback_task_create);
  register_callback(ompt_callback_thread_begin);
  register_callback(ompt_callback_thread_end);
  
  int state = omp_state_undefined;
  const char *state_name;

  while (ompt_enumerate_states(state, &state, &state_name)) {
//    printf("%i: %s\n", state, state_name);
    ompt_state_map[state] = make_pair(std::string(state_name),0);
  }

  return 1; //success
}

void ompt_finalize(ompt_data_t* data)
{
  sigset_t mask;
  printf("Blocking signal %d\n", SIG);
  sigemptyset(&mask);
  sigaddset(&mask, SIG);
  if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1)
    errExit("sigprocmask");

}

ompt_start_tool_result_t* ompt_start_tool(
  unsigned int omp_version,
  const char *runtime_version)
{
  static ompt_start_tool_result_t ompt_start_tool_result = {&ompt_initialize,&ompt_finalize,0};
  return &ompt_start_tool_result;
}
