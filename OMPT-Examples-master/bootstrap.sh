BASE=$PWD
CLANG=${CLANG:=clang}
CLANGXX=${CLANGXX:=clang++}
LLVM=$(dirname $(dirname $(which $CLANG)))

# download multiplex header
mkdir OMPT-Multiplex -p
wget https://github.com/llvm/llvm-project/raw/main/openmp/tools/multiplex/ompt-multiplex.h -O OMPT-Multiplex/ompt-multiplex.h -c


# create user specific make config file
cat $BASE/make.template > $BASE/make.config
sed -i -e "s/CC=clang/CC=$CLANG/" -e "s/CXX=clang++/CXX=$CLANGXX/" $BASE/make.config
echo OPENMP_LIBRARY=-L$LLVM/lib/ -Wl,--rpath,$LLVM/lib/ >> $BASE/make.config
echo OPENMP_INCLUDE=-I$BASE/OMPT-Multiplex >> $BASE/make.config
echo 'FLAGS_OPENMP+=$(OPENMP_LIBRARY) $(OPENMP_INCLUDE)' >> $BASE/make.config

# build all examples
for i in $BASE/example{1..4}/
do
	cd $i
	make clean && make all
done