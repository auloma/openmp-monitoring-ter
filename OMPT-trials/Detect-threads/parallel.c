#include <stdio.h>
#include <omp.h>

int main() {

    #pragma omp parallel num_threads(3)
    fprintf(stdout, "Task in parallel\n");
}