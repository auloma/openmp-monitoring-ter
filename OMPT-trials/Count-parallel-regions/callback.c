#include <assert.h>
#include <execinfo.h>
#include <inttypes.h>
#include <omp.h>
#include <omp-tools.h>
#include <stdio.h>
#include <sys/resource.h>

typedef struct datas {
  double * time;
  unsigned int * counter;
} datas, * datas_ptr;

static unsigned int counter = 0;

//static uint64_t counter;
static ompt_get_thread_data_t ompt_get_thread_data;
static ompt_get_unique_id_t ompt_get_unique_id;

#define register_callback_t(name, type)                                        \
  do {                                                                         \
    type f_##name = &on_##name;                                                \
    if (ompt_set_callback(name, (ompt_callback_t)f_##name) == ompt_set_never)  \
      printf("0: Could not register callback '" #name "'\n");                  \
  } while (0)

#define register_callback(name) register_callback_t(name, name##_t)


// Parallel regions

static void on_ompt_callback_parallel_begin(
    ompt_data_t *encountering_task_data,
    const ompt_frame_t *encountering_task_frame, ompt_data_t *parallel_data,
    uint32_t requested_parallelism, int flags, const void *codeptr_ra) {
  
  uint64_t tid = ompt_get_thread_data()->value;
  fprintf(stdout, "%ld: Parallel region begins with %u threads\n", tid, requested_parallelism);
  ++counter;

}

static void on_ompt_callback_parallel_end(ompt_data_t *parallel_data,
                                          ompt_data_t *encountering_task_data,
                                          int flags, const void *codeptr_ra) {
  uint64_t tid = ompt_get_thread_data()->value;
  fprintf(stdout, "Parallel region ends\n");
}


int ompt_initialize(ompt_function_lookup_t lookup, int initial_device_num,
                    ompt_data_t *tool_data) {
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_get_thread_data = (ompt_get_thread_data_t)lookup("ompt_get_thread_data");
  ompt_get_unique_id = (ompt_get_unique_id_t)lookup("ompt_get_unique_id");
  
  datas * dt = (datas*) tool_data->ptr;
  const double init_time = omp_get_wtime() - *dt->time;

  printf("\033[0;31m");
  fprintf(stdout, "Init time: %lf\n",init_time);
  printf("\033[0m");

  register_callback(ompt_callback_parallel_begin);
  register_callback(ompt_callback_parallel_end);

  return 1; // success: activates tool
}

void ompt_finalize(ompt_data_t *tool_data) {
  datas * dt = (datas*) tool_data->ptr;
  const double exec_time = omp_get_wtime() - *dt->time;

  printf("\033[0;31m");
  fprintf(stdout, "Exec time: %lf, parallel regions used: %u\n", exec_time, *dt->counter);
  printf("\033[0m");
}

ompt_start_tool_result_t *ompt_start_tool(unsigned int omp_version,
                                          const char *runtime_version) {

  static double time = 0; // static defintion needs constant assigment
  time = omp_get_wtime();

  static datas dt = {
    .time =  &time,
    .counter = &counter,
  };

  fprintf(stdout, "Start time: %lf\n", *dt.time);
  static ompt_start_tool_result_t ompt_start_tool_result = {
      &ompt_initialize, &ompt_finalize, {.ptr = &dt}
  };
  return &ompt_start_tool_result; // success: registers tool
}