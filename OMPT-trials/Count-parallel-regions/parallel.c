#include <stdio.h>
#include <omp.h>

int main() {


    omp_set_nested(1);
    #pragma omp parallel num_threads(5)
    {
        fprintf(stdout, "Task in parallel\n");
        #pragma omp parallel
        fprintf(stdout, "Secondary task in parallel\n");
    }

    #pragma omp parallel num_threads(3)
    fprintf(stdout, "Another task in parallel\n");
}