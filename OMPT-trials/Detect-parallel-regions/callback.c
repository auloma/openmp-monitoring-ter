#include <assert.h>
#include <execinfo.h>
#include <inttypes.h>
#include <omp.h>
#include <omp-tools.h>
#include <stdio.h>
#include <sys/resource.h>

#define register_callback_t(name, type)                                        \
  do {                                                                         \
    type f_##name = &on_##name;                                                \
    if (ompt_set_callback(name, (ompt_callback_t)f_##name) == ompt_set_never)  \
      printf("0: Could not register callback '" #name "'\n");                  \
  } while (0)

#define register_callback(name) register_callback_t(name, name##_t)


// Parallel regions

static void on_ompt_callback_parallel_begin(
    ompt_data_t *encountering_task_data,
    const ompt_frame_t *encountering_task_frame, ompt_data_t *parallel_data,
    uint32_t requested_parallelism, int flags, const void *codeptr_ra) {
  //uint64_t tid = ompt_get_thread_data()->value;
  // if (flags & ompt_parallel_league) {
  //   counter[tid].cc.teams_begin += 1;
  // } else if (flags & ompt_parallel_team) { // only 'else' sufficient
  //   counter[tid].cc.parallel_begin += 1;
  // }
  // parallel_data->value = ompt_get_unique_id();
  fprintf(stdout, "Parallel region begins\n");
}

static void on_ompt_callback_parallel_end(ompt_data_t *parallel_data,
                                          ompt_data_t *encountering_task_data,
                                          int flags, const void *codeptr_ra) {
  // uint64_t tid = ompt_get_thread_data()->value;
  // if (flags & ompt_parallel_league) {
  //   counter[tid].cc.teams_end += 1;
  // } else if (flags & ompt_parallel_team) { // only 'else' sufficient
  //   counter[tid].cc.parallel_end += 1;
  // }
  fprintf(stdout, "Parallel region ends\n");
}


int ompt_initialize(ompt_function_lookup_t lookup, int initial_device_num,
                    ompt_data_t *tool_data) {
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  
  printf("libomp init time: %f\n",
         omp_get_wtime() - *(double *)(tool_data->ptr));
  *(double *)(tool_data->ptr) = omp_get_wtime();

  register_callback(ompt_callback_parallel_begin);
  register_callback(ompt_callback_parallel_end);
  
  return 1; // success: activates tool
}

void ompt_finalize(ompt_data_t *tool_data) {
  printf("application runtime: %f\n",
         omp_get_wtime() - *(double *)(tool_data->ptr));
}

ompt_start_tool_result_t *ompt_start_tool(unsigned int omp_version,
                                          const char *runtime_version) {
  static double time = 0; // static defintion needs constant assigment
  time = omp_get_wtime();
  static ompt_start_tool_result_t ompt_start_tool_result = {
      &ompt_initialize, &ompt_finalize, {.ptr = &time}
  };
  return &ompt_start_tool_result; // success: registers tool
}