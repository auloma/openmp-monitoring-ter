#ifndef __RECORD_H__
#define __RECORD_H__

#ifndef _BSD_SOURCE
#define _BSD_SOURCE
#endif
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#include <stdio.h>
#include <stdbool.h>
#ifndef __STDC_FORMAT_MACROS
#define __STDC_FORMAT_MACROS
#endif
#include <inttypes.h>
#include <omp.h>
#include <omp-tools.h>


typedef enum event_type_t {
  event_thread                        = 1,
  event_parallel                      = 2,
  event_barrier                       = 3,
  event_task                          = 4,
  event_work                          = 5,
} event_type_t;


typedef struct event {
    event_type_t event_type;
    int thread_id;
    ompt_id_t thread_id_inteam;
    int requested_team_size; /* team size requested by the user*/
    int team_size;           /* the actual team size set by the runtime */
    ompt_scope_endpoint_t endpoint; /* begin or end */
    const void *codeptr_ra;  /* for *_begin event, codeptr_ra is the same as the lgp codeptr_ra,
                              * for *_end event, it is the address of the end of lexgion, ideally */
    ompt_id_t parent; /* the event for the lexgion that enclose the lexgion for this event */
    ompt_id_t parallel_id;
    ompt_id_t new_task_id;
    struct ompt_trace_event *task; /* the event for the enclosing task of this event */
    struct ompt_trace_event *parallel_event; /* The event for the innermost parellel lexgion that encloses this event */

    int event_id;
    int match_event; /* index for the matching event. the match for begin_event is end and the match for end_event is begin */
    struct event **parallel_implicit_tasks; /* an array for parallel event to store the pointers to all the events of implicit tasks */
    struct event **parallel_implicit_barrier_sync; /* the events for sync_barrier_region of all the team threads */
    struct event **parallel_implicit_barrier_wait; /* the events for sync_barrier_wait of all the team threads */

    struct event_t *next; /* the link of the link list for all the begin/start events of the same lexical region */
} event_t;


/**
 * history of events by thread
 **/
typedef struct thread_hist {
    int thread_id;
    ompt_data_t *thread_data;
    event_t *events;
} thread_hist_t;



event_t * add_parallel_event(int thread_id, /* thread_num : 0 ..... */
		       event_type_t type, /* type of event (my definition)*/
                   bool is_begin,  /* bool : is a begin or end event */
                   ompt_id_t parent, /* parent task (ompt_id_t)*/
                   ompt_id_t parallel_id,    /* parallel id (ompt_id_t) */
                   size_t requested_team_size);

event_t * add_taskcreate_event(int thread_id, /* thread_num : 0 ..... */
		       event_type_t type, /* type of event (my definition)*/
                   ompt_id_t parent, /* parent task (ompt_id_t)*/
                   ompt_id_t new_task_id,    /* new task id (ompt_id_t) */
                   bool has_dependences); 

void print_record(size_t tick, event_t *evt);
#endif
