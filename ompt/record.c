#include "record.h"
#include <omp-tools.h>

event_t * add_parallel_event(int thread_id, /* thread_num : 0 ..... */
		  	 event_type_t type, /* type of event (my definition)*/
                   bool is_begin,  /* bool : is a begin or end event */
                   ompt_id_t parent, /* parent task (ompt_id_t)*/
                   ompt_id_t parallel_id,    /* parallel id (ompt_id_t) */
                   size_t requested_team_size)
{
    	event_t *evt = malloc(sizeof(event_t));
      evt->thread_id = thread_id;
	evt->event_type = type;
      evt->requested_team_size = requested_team_size; /* team size requested by the user*/
      evt->parent = parent;
      evt->parallel_id = parallel_id;
	if (is_begin)
		  evt->endpoint = ompt_scope_begin;
	else
		  evt->endpoint = ompt_scope_end;
	return evt;
}


event_t * add_taskcreate_event(int thread_id, /* thread_num : 0 ..... */
		    		 event_type_t type, /* type of event (my definition)*/
                         ompt_id_t parent, /* parent task (ompt_id_t)*/
                         ompt_id_t new_task_id,    /* new task id (ompt_id_t) */
                         bool has_dependences) 
{
    	event_t *evt = malloc(sizeof(event_t));
      evt->thread_id = thread_id;
	evt->event_type = type;
      evt->parent = parent;
      evt->new_task_id = new_task_id;
	//evt->has_dependences = has_dependences;  not yet defined in struct. Do we need it ?
	return evt;
}


void print_record(size_t tick, event_t *evt) {
	printf("%zu [%d]",tick, evt->thread_id);
	printf("type=%i\n", evt->event_type);
	switch(evt->event_type) {
		  case event_thread:
			printf("thread\n");
		  break;
		  case event_parallel:
			printf("parallel\n");
		  break;
		  case event_barrier:
			printf("barrier\n");
		  break;
		  case event_task:
			printf("task\n");
		  break;
		  case event_work:
			printf("work\n");
		  break;
		  default:
		  printf("no type found\n");
	}
	printf("--\n");
}

